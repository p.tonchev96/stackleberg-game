import comp34120.ex2.Matrix;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MatrixTest {

    @Test
    public void testMatrixWithVectorMultiplication() {
        /*
         |1 2|  multiplied by |5|  is equal to |17|
         |3 4|                |6|              |39|
         */
        assertArrayEquals(new double[] {17,39}, Matrix.multiply(new double[][] {{1,2}, {3,4}}, new double[] {5,6}), 0);
    }

    @Test
    public void testTransposedVectorWithMatrixMultiplication() {
        /*
         |1 2|  multiplied by |3 4|  is equal to |13 16|
                              |5 6|
         */
        assertArrayEquals(new double[] {13,16}, Matrix.multiply(new double[] {1,2}, new double[][] {{3,4},{5,6}}), 0);
    }

    @Test
    public void testTransposedVectorWithVectorMultiplication() {
        /*
         |1 2|  multiplied by |3|  is equal to |11|
                              |4|
         */
        assertEquals(11, Matrix.multiplyHV(new double[] {1,2}, new double[] {3,4}), 0);
    }

    @Test
    public void testVectorWithTransposedVectorMultiplication() {
        /*
         |1|  multiplied by |3 4|  is equal to |3 4|
         |2|                                   |6 8|
         */
        double[][] result = Matrix.multiplyVH(new double[] {1,2}, new double[] {3,4});
        assertArrayEquals(new double[] {3,4}, result[0], 0);
        assertArrayEquals(new double[] {6,8}, result[1], 0);
    }

    @Test
    public void testMatrixWithMatrixMultiplication() {
        /*
         |1 2|  multiplied by |5 6|  is equal to |19 22|
         |3 4|                |7 8|              |43 50|
         */
        double[][] result = Matrix.multiply(new double[][] {{1,2}, {3,4}}, new double[][] {{5,6}, {7,8}});
        assertArrayEquals(new double[] {19,22}, result[0], 0);
        assertArrayEquals(new double[] {43,50}, result[1], 0);
    }

    @Test
    public void testVectorWithScalarMultiplication() {
        /*
         |1 2|  multiplied by |2|  is equal to |2 4|
         */
        assertArrayEquals(new double[] {2,4}, Matrix.multiply(new double[] {1,2}, 2), 0);
    }

    @Test
    public void testMatrixWithScalarMultiplication() {
        /*
         |1 2|  multiplied by |5|  is equal to | 5 10|
         |3 4|                                 |15 20|
         */
        double[][] result = Matrix.multiply(new double[][] {{1,2}, {3,4}}, 5);
        assertArrayEquals(new double[] {5,10}, result[0], 0);
        assertArrayEquals(new double[] {15,20}, result[1], 0);

        result = Matrix.multiply(5, new double[][] {{1,2}, {3,4}});
        assertArrayEquals(new double[] {5,10}, result[0], 0);
        assertArrayEquals(new double[] {15,20}, result[1], 0);
    }

    @Test
    public void testMatrixWithScalarDivision() {
        /*
         | 5 10|  divided by |5|  is equal to |1 2|
         |15 20|                              |3 4|
         */
        double[][] result = Matrix.divide(new double[][] {{5,10}, {15,20}}, 5);
        assertArrayEquals(new double[] {1,2}, result[0], 0);
        assertArrayEquals(new double[] {3,4}, result[1], 0);
    }

    @Test
    public void testMatrixWithMatrixSubtraction() {
        /*
         |1 2|  subtracted from | 5 10|  is equal to | 4  8|
         |3 4|                  |15 20|              |12 16|
         */
        double[][] result = Matrix.subtract(new double[][] {{5,10}, {15,20}}, new double[][] {{1,2}, {3,4}});
        assertArrayEquals(new double[] {4,8}, result[0], 0);
        assertArrayEquals(new double[] {12,16}, result[1], 0);
    }

    @Test
    public void testVectorWithVectorAddition() {
        /*
         |1 2|  added to |3 4|  is equal to |4  6|
         */
        assertArrayEquals(new double[] {4,6}, Matrix.add(new double[] {1,2}, new double[] {3,4}), 0);
    }

    @Test
    public void testVectorWithScalarDivision() {
        /*
         |6 9|  divided with |3|  is equal to |2  3|
         */
        assertArrayEquals(new double[] {2,3}, Matrix.divide(new double[] {6,9}, 3), 0);
    }
}
