package comp34120.ex2;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class LeaderTest {
    private PseudoLeader leader;
    private double[][] P;
    private double[] phi;
    private double lambda, y;

    @Before
    public void setUp() throws RemoteException, NotBoundException {
        leader = Mockito.mock(PseudoLeader.class);

        P = new double[][] {{1,2},{3,4}};
        phi = new double[] {1.8, 1.9};
        lambda = 1;
        y = 1.8;
    }

    @Test
    public void testUpdateP() {
        when(leader.updateP(P, phi, lambda)).thenCallRealMethod();

        double[][] result = leader.updateP(P, phi, lambda);
        assertArrayEquals(new double[] {-0.17384,0.24707}, result[0], 0.00001);
        assertArrayEquals(new double[] {0.27501,-0.06931}, result[1], 0.00001);
    }

    @Test
    public void calcAdjustingFactor() {
        when(leader.calcAdjustingFactor(P, phi, lambda)).thenCallRealMethod();

        double[] result = leader.calcAdjustingFactor(P, phi, lambda);
        assertArrayEquals(new double[] {0.156512,0.363331}, result, 0.000001);
    }

    @Test
    public void updateTheta() {
        when(leader.updateTheta(P, phi, lambda, y)).thenCallRealMethod();
        doReturn(new double[] {0.2,0.3}).when(leader).calcAdjustingFactor(P, phi, lambda);
        doReturn(new double[] {1.5,0.5}).when(leader).getTheta();

        double[] result = leader.updateTheta(P, phi, lambda, y);
        assertArrayEquals(new double[] {1.13,-0.055}, result, 0.001);
    }
}
