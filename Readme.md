# Stackeberg Game Simulation 

A project created for AI and Games Course in UoM.



## Overview of The Task

Our task is to make a program which plays repeated 2-person Stackelberg pricing games as the leader under imperfect information. 

**Specifications about the leader**: 
Suppose that the leader’s strategy (i.e., price) is $`u_L`$ and the follower’s strategy (i.e., price) is $`u_F`$, then the detailed specifications about the leader are given as follows: 
* Unit cost: $`c_L = 1.00`$
* Strategy space: $`U_L = [1.00, + \infty]`$
* Demand Model (i.e., price-sale relationship): $`S_L(u_L, u_F) = 2 - u_L + 0.3*u_F`$
* Daily Profit: $`(u_L - c_L)S_L(u_L,u_F)`$
* The leader has no information about the follower’s strategy space and payoff function but there are a set of historical data (100 days) being provided by the game platform
* The leader’s objective of playing is to maximise his accumulated profit for the next 30 days
* For the simplicity, it is assumed that the leader’s unit cost, the strategy space, and the demand model are unchanged during the whole period (130 days).

## Rules of playing

At day $`t`$, the leader will announce his price $`u_L(t)`$ first;
 Knowing the leader’s price, the follower will choose his responding price $`u_F(t)`$ , where $`t=1,2,...,130`$ , in which $`t=1,2,...,100`$ are for historical data, whereas $`t=101,...,130`$ are the days when the game is going to play repeatedly.

## Game playing scenarios:
The leader (i.e., your program) is going to play three separated games in which a different competitor (i.e., the follower) is to be faced. The three different followers are called MK1, MK2, and MK3 respectively. In each of the repeated game, the leader chooses his learning method and strategy to play based on the provided set of historical data. Then each day during the playing period (i.e., the next 30 days), the leader will send his price $`u_L(t)`$ to the game platform, and the follower will give his response $`u_F(t)`$. After receiving the follower’s responding price $`u_F(t)`$ from the game platform, the leader can use this piece of information to update his knowledge and decide his price of the next day. In the other words, the leader (i.e., your program) should be able to take the follower’s price of the previous day from the game platform for updating, and then decide and send his price of the next day to the game platform. 
It should be emphasised that the follower’s strategy and payoff function is subject to the changing and time varying environment (that is, the parameters in the follower’s payoff function are not the same every day). 





## Using the Platform:


### 1.Introduction


This experiment platform is implemented using Java SE 6 with RMI, so that you can implement your own Java classes and run them without knowing the detail of the platform.

To do so, you need to implement a leader class by extending the abstract class PlayerImpl in package comp34120.ex2, and some essential source codes are under /src.


###  2.Running the demo 

A very simple example of leader class, SimpleLeader, has been provided, and its source code is under /src as well.

To run the simulation with the SimpleLeader, you need to

1.  enable RMI registration
```bash
/usr/java/latest/bin/rmiregistry &
```
2.  run the GUI of the platform
```bash
java -classpath poi-3.7-20101029.jar: -Djava.rmi.server.hostname=127.0.0.1 comp34120.ex2.Main &
```
3. run the SimpleLeader
```bash
java -Djava.rmi.server.hostname=127.0.0.1 SimpleLeader &
```

And after these steps, you can play with the GUI to get some ideas of how the platform works.


###  3.Implementing your own leader class 

#### 3.1. How does the platform work

The platform is like a referee of the game. Every step of the simulation begins after the platform informing every player the beginning of a new day. And the players need to ask the platform for the price information, and finally submit a new price of the day to the platform to end its phase.

#### 3.2. The platform interface

There are five method that platform provides for the players to call: (YOU DO NOT NEED TO IMPLEMENT THEM, THEY ARE PROVIDED BY THE PLATFORM)

##### 3.2.1. Checking the status of the RMI connection

public void checkConnection(final PlayerType p_type)
	throws RemoteException;

It simply does nothing. If the connection fails, the JVM will throw a RemoteException, so that you know the failure of the connection. Normally nothing will be done by this method. You may not need this method. The platform provide this just in case you want to make your leader program more robust.

##### 3.2.2. Registering to the platform
```java
public void registerPlayer(final PlayerType p_type, final String p_displayName) throws RemoteException, NotBoundException;
```

Register this leader instance to the platform. Normally you will not need to call this method because RMI registeration procedure has already been implemented in the abstract class PlayerImpl which you need to extend to implement your own leader. This method will be called automatically in the constructor of the PlayerImpl which you need to call in the first line of the constructor of your own leader class.

##### 3.2.3. Get the information of price and cost
```java
public Record query(final PlayerType p_type,
	final int p_queryDate)
	throws RemoteException;
```

By calling this method, you can get the price of both the leader and the follower as well as the cost of a given day. The Record is a public class which contains the price and cost in it. This is a very important method you may want to use in your own leader.

##### 3.2.4. Publish your new price
```java
public void publishPrice(final PlayerType p_type,
	final float p_price)
	throws RemoteException;
```
Publish your new price by calling this method. After calculating your profit and get a optimal price, you need to call this method to finish your phase of this simulation day.

##### 3.2.5. Output some information to the GUI
```java
public void log(final PlayerType p_type,
	final String p_text)
	throws RemoteException;
```
By calling this method you can publish some information (e.g. debug information) on the GUI.

### 3.3. The player interface

##### 3.3.1. Check the status of the RMI connection
```java
public void checkConnection()
	throws RemoteException;
```
##### Similar to the method of the platform, this method does nothing, just for detection of connection failure. You do not need to override this method.

##### 3.3.2. Inform the end of the program
```java
public void goodbye()
	throws RemoteException;
```
The platform will call this method when you close the GUI of the platform. An example of the use of this method can be found in SimpleLeader, after calling which the program exits. You may make the choice whether override this method or not.

##### 3.3.3. Start the simulation
```java
public void startSimulation(final int p_steps)
	throws RemoteException;
```
The platform will call this method when the simulation begins. You may want to do some initialization here, otherwise you don't need to override it.

##### 3.3.4. End the simulation
```java
public void endSimulation()
	throws RemoteException;
```
The platform will call this method when the simulation ends. You may want to do some finalization here, otherwise you don't need to override it.

##### 3.3.5. Proceed to a new day
```java
public void proceedNewDay(final int p_date)
	throws RemoteException;
```
Platform will call this method when the simulation proceeds to a new day. This is the only method you *MUST* implement. Here you may want to ask the platform of the price and cost of the day before the current day. You may also put all your math stuff here to compute an optimal price. Your phase will end after your done everything here.

