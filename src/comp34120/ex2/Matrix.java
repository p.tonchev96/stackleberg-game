package comp34120.ex2;

public class Matrix {

    /**
     * Multiplies the given vector and matrix using vanilla for loops.
     *
     * @param matrix the matrix
     * @param vector the vector to multiplyHV
     *
     * @return result after multiplication.
     */
    public static double[] multiply(double[][] matrix, double[] vector) {

        return new double[] {
                matrix[0][0] * vector[0] + matrix[0][1] * vector[1],
                matrix[1][0] * vector[0] + matrix[1][1] * vector[1]
        };
    }

    /**
     * Multiplies the given vector and matrix using vanilla for loops.
     *
     * @param vector the vector to multiplyHV
     * @param matrix the matrix
     *
     * @return result after multiplication.
     */
    public static double[] multiply(double[] vector, double[][] matrix) {

        return new double[] {
                vector[0] * matrix[0][0] + vector[1] * matrix[1][0],
                vector[0] * matrix[0][1] + vector[1] * matrix[1][1]
        };
    }

    /**
     * Multiplies the given vector and matrix using vanilla for loops.
     *
     * @param vector1 the vector to multiplyHV
     * @param vector2 the matrix
     *
     * @return result after multiplication.
     */
    public static double multiplyHV(double[] vector1, double[] vector2) {

        return vector1[0] * vector2[0] + vector1[1] * vector2[1];
    }

    public static double[][] multiplyVH(double[] vector1, double[] vector2) {

        return new double[][] {
                {vector1[0] * vector2[0], vector1[0] * vector2[1]},
                {vector1[1] * vector2[0], vector1[1] * vector2[1]}
        };
    }

    public static double[][] multiply(double[][] matrix1, double[][] matrix2) {

        return new double[][] {
                {
                    matrix1[0][0] * matrix2[0][0] + matrix1[0][1] * matrix2[1][0],
                    matrix1[0][0] * matrix2[0][1] + matrix1[0][1] * matrix2[1][1]
                },
                {
                    matrix1[1][0] * matrix2[0][0] + matrix1[1][1] * matrix2[1][0],
                    matrix1[1][0] * matrix2[0][1] + matrix1[1][1] * matrix2[1][1]
                }
        };
    }

    public static double[] multiply(double[] vector, double value) {
        double[] result = new double[vector.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = vector[i] * value;
        }
        return result;
    }

    public static double[][] multiply(double[][] matrix, double value) {
        double[][] result = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = matrix[i][j] * value;
            }
        }
        return result;
    }

    public static double[][] divide(double[][] matrix, double value) {
        double[][] result = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = matrix[i][j] / value;
            }
        }
        return result;
    }

    public static double[][] subtract(double[][] matrix1, double[][] matrix2) {
        double[][] result = new double[matrix1.length][matrix1[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = matrix1[i][j] - matrix2[i][j];
            }
        }
        return result;
    }

    public static double[][] add(double[][] matrix1, double[][] matrix2) {
        double[][] result = new double[matrix1.length][matrix1[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return result;
    }

    public static double[][] multiply(double value, double[][] matrix) {
        return multiply(matrix, value);
    }

    public static double[] add(double[] vector1, double[] vector2) {
        double[] result = new double[vector1.length];

        for (int i = 0; i < vector1.length; i++) {
            result[i] = vector1[i] + vector2[i];
        }
        return result;
    }

    public static double[] divide(double[] vector, double value) {
        double[] result = new double[vector.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = vector[i] / value;
        }
        return result;
    }

    public static double[][] inverse(double[][] matrix) {
        double[][] result = new double[2][2];

        result[0][0] = matrix[1][1];
        result[0][1] = -matrix[0][1];
        result[1][0] = -matrix[1][0];
        result[1][1] = matrix[0][0];

        double determinant = 1 / (matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]);

        return Matrix.multiply(result, determinant);
    }
}
