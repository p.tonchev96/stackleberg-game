package comp34120.ex2;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * A pseudo leader. The members m_platformStub and m_type are declared
 * in the PlayerImpl, and feel free to use them. You may want to check
 * the implementation of the PlayerImpl. You will use m_platformStub to access
 * the platform by calling the remote method provided by it.
 *
 * @author Xin
 */
class PseudoLeader
        extends PlayerImpl {

    protected float totalProfit = 0;

    //    final protected double[] theta_0 = {1.1305950346586415, 0.35720162};
    protected double[] theta;
    protected double[][] P;
    protected double lambda = 0.97;


    // Used for evaluation
    protected double[] predictedPrices = new double[30];

    /**
     * In the constructor, you need to call the constructor
     * of PlayerImpl in the first line, so that you don't need to
     * care about how to connect to the platform. You may want to throw
     * the two exceptions declared in the prototype, or you may handle it
     * by using "try {} catch {}". It's all up to you.
     *
     * @throws RemoteException
     * @throws NotBoundException
     */
    PseudoLeader()
            throws RemoteException, NotBoundException {
        /* The first parameter *MUST* be PlayerType.LEADER, you can change
         * the second parameter, the name of the leader, such as "My Leader" */
        super(PlayerType.LEADER, "Leader Group 19");
    }

    /**
     * You may want to delete this method if you don't want modify
     * the original connection checking behavior of the platform.
     * Actually I recommend you to delete this method from your own code
     *
     * @throws RemoteException If implemented, the RemoteException *MUST* be
     *                         thrown by this method
     */
    @Override
    public void checkConnection()
            throws RemoteException {
        super.checkConnection();
        //TO DO: delete the line above and put your own code here
    }

    /**
     * You may want to delete this method if you don't want the platform
     * to control the exit behavior of your leader class
     *
     * @throws RemoteException If implemented, the RemoteException *MUST* be
     *                         thrown by this method
     */
    @Override
    public void goodbye()
            throws RemoteException {
        super.goodbye();
        //TO DO: delete the line above and put your own exit code here
    }

    /**
     * You may want to delete this method if you don't want to do any
     * initialization
     *
     * @param p_steps Indicates how many steps will the simulation perform
     * @throws RemoteException If implemented, the RemoteException *MUST* be
     *                         thrown by this method
     */
    @Override
    public void startSimulation(int p_steps)
            throws RemoteException {
        super.startSimulation(p_steps);
        calcBestInitialCondition();
        //TO DO: delete the line above and put your own initialization code here
    }

    /**
     * You may want to delete this method if you don't want to do any
     * finalization
     *
     * @throws RemoteException If implemented, the RemoteException *MUST* be
     *                         thrown by this method
     */
    @Override
    public void endSimulation()
            throws RemoteException {
        super.endSimulation();

        totalProfit = 0;
//        Uncomment for evaluation:
//        evaluatePerformance();
    }

    protected void evaluatePerformance() throws RemoteException {

        Record currRecord;
        double[] followerPrices = new double[30];

        for (int day = 101; day <= 130; day++) {
            currRecord = m_platformStub.query(this.m_type, day);
            followerPrices[day - 101] = currRecord.m_followerPrice;
        }
        double mape = getMAPE(followerPrices);
        double rmse = getRMSE(followerPrices);
        this.m_platformStub.log(this.m_type, "MAPE score for 30 days is: " + mape);
        this.m_platformStub.log(this.m_type, "RMSE score for 30 days is: " + rmse);
    }

    private double getRMSE(double[] followerPrices) {

        double sumOfSquaredError = 0;
        for (int i = 0; i < followerPrices.length; i++) {
            sumOfSquaredError += Math.pow(followerPrices[i] - predictedPrices[i], 2);
        }

        return Math.sqrt(sumOfSquaredError / followerPrices.length);
    }

    private double getMAPE(double[] followerPrices) {
        double sumOfPercentageError = 0;
        for (int i = 0; i < followerPrices.length; i++) {
            sumOfPercentageError += Math.abs((followerPrices[i] - predictedPrices[i]) / followerPrices[i]);
        }

        return sumOfPercentageError / followerPrices.length;
    }

    private double getR2(double[] followerPrices) {
        // 1. Calc Mean Follower Price
        double meanFollowerPrice = calcMeanFollowerPrice(followerPrices);
        //---------------------------------
        // 2. Calc total sum of squares and sum of squared error
        // SSt = sum( [ y(t) - mean(y)]^2 )
        // SSe = sum( [ y(t) - y_hat]^2 )
        double totalSumOfSquares = 0;
        double sumOfSquaredError = 0;
        for (int i = 0; i < followerPrices.length; i++) {
            totalSumOfSquares += Math.pow(followerPrices[i] - meanFollowerPrice, 2);
            sumOfSquaredError += Math.pow(followerPrices[i] - predictedPrices[i], 2);
        }
        // 3. Calc r2 = 1 - SSe/SSt
        return 1 - (sumOfSquaredError / totalSumOfSquares);
    }

    private double calcMeanFollowerPrice(double[] followerPrices) {
        double followerSumOfPrices = 0;
        for (int i = 0; i < followerPrices.length; i++) {
            followerSumOfPrices += followerPrices[i];
        }
        return followerSumOfPrices / followerPrices.length;
    }

    /**
     * To inform this instance to proceed to a new simulation day
     *
     * @param p_date The date of the new day
     * @throws RemoteException This exception *MUST* be thrown by this method
     */
    @Override
    public void proceedNewDay(int p_date)
            throws RemoteException {

        Record lastDayRecord = m_platformStub.query(this.m_type, p_date - 1);

        float currentLeaderPrice = calcBestPrice(p_date);

        m_platformStub.publishPrice(m_type, currentLeaderPrice);

        // Save predicted prices for evaluation
        int currentIndex = p_date - 101;
        predictedPrices[currentIndex] = theta[0] + (theta[1] * currentLeaderPrice);
    }


    protected double[] recursiveLeastSquare(int day) throws RemoteException {
        Record currentRecord;
        double[] phi;
        double[] theta = getTheta();

//        if (day > 100) {
        currentRecord = m_platformStub.query(this.m_type, day - 1);
        //TODO: Is phi follower or leader price?
        phi = new double[]{1, currentRecord.m_leaderPrice};
        theta = updateTheta(P, phi, lambda, currentRecord.m_followerPrice);
        setTheta(theta);
        P = updateP(P, phi, lambda);
//        }
//
//        for (int i = 1; i < 100; i++) {
//            currentRecord = m_platformStub.query(this.m_type, i);
//            phi = new double[] {1, currentRecord.m_followerPrice};
//            P = updateP(P, phi, lambda);
//            theta = updateTheta(P, phi, lambda, currentRecord.m_leaderPrice);
//            setTheta(theta);
//        }

        return theta;
    }

    protected double[][] updateP(double[][] P, double[] phi, double lambda) {
        double[][] dividend = Matrix.multiply(Matrix.multiplyVH(Matrix.multiply(P, phi), phi), P);
        double divisor = lambda + Matrix.multiplyHV(Matrix.multiply(phi, P), phi);
        double[][] quotient = Matrix.divide(dividend, divisor);
        return Matrix.divide(Matrix.subtract(P, quotient), lambda);
    }

    protected double[] updateTheta(double[][] P, double[] phi, double lambda, double y) {
        double[] adjustingFactor = calcAdjustingFactor(P, phi, lambda);
        double predictionError = y - Matrix.multiplyHV(phi, getTheta());
        return Matrix.add(getTheta(), Matrix.multiply(adjustingFactor, predictionError));
    }

    protected double[] calcAdjustingFactor(double[][] P, double[] phi, double lambda) {
        double[] dividend = Matrix.multiply(P, phi);
        double divisor = lambda + Matrix.multiplyHV(Matrix.multiply(phi, P), phi);

        return Matrix.divide(dividend, divisor);
    }

    protected double[][] calcInitialCondition() {
        double C = Integer.MAX_VALUE;
        return new double[][]{{1 / C, 0}, {0, 1 / C}};
    }

    protected void calcBestInitialCondition() throws RemoteException {
        int T_0 = 100;
        P = calcInitialP(T_0);
        theta = calcInitialTheta(T_0);
    }

    private double[] calcInitialTheta(int t_0) throws RemoteException {
        Record currentRecord;
        double[] sumOfVectors = new double[2];

        for (int day = 1; day <= t_0; day++) {
            currentRecord = m_platformStub.query(this.m_type, day);
            double[] phi = {1, currentRecord.m_leaderPrice};
            double currLambda = Math.pow(lambda, t_0 - day);
            sumOfVectors = Matrix.add(sumOfVectors, Matrix.multiply(phi, currLambda * currentRecord.m_followerPrice));
        }

        double[][] P_inverse = Matrix.inverse(P);
        return Matrix.multiply(P_inverse, sumOfVectors);
    }

    private double[][] calcInitialP(int t_0) throws RemoteException {
        Record currentRecord;
        double[][] P_t0 = new double[2][2];

        for (int day = 1; day <= t_0; day++) {
            currentRecord = m_platformStub.query(this.m_type, day);

            double currLambda = Math.pow(lambda, t_0 - day);
            double[] phi = {1, currentRecord.m_leaderPrice};
            double[][] currentDayValue = Matrix.multiply(currLambda, Matrix.multiplyVH(phi, phi));
            P_t0 = Matrix.add(P_t0, currentDayValue);
        }
        return P_t0;
    }


    protected float maximizeLeaderProfit(double[] theta, int day) throws RemoteException {
        double x = theta[0];
        double y = theta[1];

        float a1 = 2f;
        float c1 = 0.3f;
        float b1 = -1f;

        float cl = 1f;

        double localMax = (-1 * (a1 + c1 * x - b1 * cl - c1 * y * cl) / (2 * b1 + 2 * c1 * y));

        Record lastDayRecord = m_platformStub.query(this.m_type, day - 1);
        double localMaxPayoff = ((localMax - 1) * (2 - localMax + 0.3 * lastDayRecord.m_followerPrice));
        double lowerBoundPayoff = 0;

        if (localMaxPayoff > lowerBoundPayoff) {
            return (float) localMax;
        } else {
            return 1;
        }
    }

    protected float calcBestPrice(int day) throws RemoteException {
//        recursiveLeastSquare(day);
        return maximizeLeaderProfit(recursiveLeastSquare(day), day);
    }

    protected double[] getTheta() {
        return theta;
    }

    protected void setTheta(double[] theta) {
        this.theta = theta;
    }

    public static void main(final String[] p_args)
            throws RemoteException, NotBoundException {
        new PseudoLeader();
    }
}
