package comp34120.ex2;

public class MK1 {

    float[][] m_paras = {
            {3, 2, 0.1f, -0.5f},
            {3, 0.3f, 0.05f, -0.5f},
            {3, -1, 0.05f, -0.5f},
    };

    float[] disturbance = {
            0.638459486f,
            0.69637487f,
            0.074461416f,
            0.164718004f,
            0.203808379f,
            0.43103423f,
            0.90409709f,
            0.097919739f,
            0.718338899f,
            0.413343792f,
            0.90549516f,
            0.934202907f,
            0.279773461f,
            0.633069456f,
            0.709531525f,
            0.361005025f,
            0.396388362f,
            0.600788731f,
            0.900644369f,
            0.502274003f,
            0.320688097f,
            0.350253595f,
            0.307134751f,
            0.057722364f,
            0.028672745f,
            0.760910526f,
            0.74400696f,
            0.928235165f,
            0.337112974f,
            0.661634984f
    };

    public float getPrice(int p_date, float leaderPrice, float followerCost) {
        float l_rand = disturbance[p_date - 101];
        float l_a = this.m_paras[0][0] + this.m_paras[0][1] * (l_rand + this.m_paras[0][2]);
        float l_b = this.m_paras[1][0] + this.m_paras[1][1] * (l_rand + this.m_paras[1][2]);
        float l_c = this.m_paras[2][0] + this.m_paras[2][1] * (l_rand + this.m_paras[2][2]);
        return (l_a + l_b * leaderPrice - followerCost * l_c) / -2.0F / l_c;
    }

    public double getLeaderPrice(int date, float followerCost) {
        float l_rand = disturbance[date - 101];
        float a = this.m_paras[0][0] + this.m_paras[0][1] * (l_rand + this.m_paras[0][2]);
        float b = this.m_paras[1][0] + this.m_paras[1][1] * (l_rand + this.m_paras[1][2]);
        float c = this.m_paras[2][0] + this.m_paras[2][1] * (l_rand + this.m_paras[2][2]);
        double divisible = -6 * c + 0.3 * a - 0.3 * c * followerCost;
        double divisor = -4 * c - 0.3 * b;
        return divisible / divisor;
    }

    public double getUlStar(int date, float followerCost) {
        float l_rand = disturbance[date - 101];
        float a = this.m_paras[0][0] + this.m_paras[0][1] * (l_rand + this.m_paras[0][2]);
        float b = this.m_paras[1][0] + this.m_paras[1][1] * (l_rand + this.m_paras[1][2]);
        float c = this.m_paras[2][0] + this.m_paras[2][1] * (l_rand + this.m_paras[2][2]);

        double beta0 = ((0.3 * (a - followerCost * c)) / (2 * c)) * -1;
        double beta1 = -1 - ((0.3 * b) / (2 * c));

        double uL = ((2 + beta0 - followerCost * beta1) / (2 * beta1)) * -1;
        return uL;
    }
}
